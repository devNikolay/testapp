package estafeta.org.testapp.network;

import java.util.List;

import estafeta.org.testapp.pojos.Trip;
import retrofit.Callback;
import retrofit.http.GET;

public interface NetworkService {
    @GET("/amt/api/mobilesurveytasks/gettabletsurveytasks?userId=null&effectivePlannedStartDate=null&expirationPlannedStartDate=null&startRowVersion=0&stateId=4&count=100")
    void getTrips(Callback<List<Trip>> callback);
}

