package estafeta.org.testapp.network;

import android.util.Base64;

import java.util.List;

import estafeta.org.testapp.pojos.Trip;
import lombok.Data;
import lombok.experimental.Accessors;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

@Data
@Accessors(prefix = "m")
public class NetworkManager {

    public static final String ENDPOINT = "http://rem.estafeta.org:9081";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "1";
    private static final String COMPANY_ID = "9F346DDB-8FF8-4F42-8221-6E03D6491756";
    private final NetworkService mService;
    private static NetworkManager sInstance;

    private NetworkManager() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        String credentials = LOGIN + "@" + COMPANY_ID + ":" + PASSWORD;
                        String header = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                        request.addHeader("Authorization", header);
                    }
                })
                .setEndpoint(ENDPOINT).build();

        mService = restAdapter.create(NetworkService.class);
    }

    public static NetworkManager get() {
        if (sInstance == null) {
            sInstance = new NetworkManager();
        }
        return sInstance;
    }

    public void loadTrips(Callback<List<Trip>> callback) {
        mService.getTrips(callback);
    }
}
