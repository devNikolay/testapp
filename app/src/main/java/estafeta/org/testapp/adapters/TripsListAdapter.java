package estafeta.org.testapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import estafeta.org.testapp.R;
import estafeta.org.testapp.pojos.Trip;

public class TripsListAdapter extends BaseAdapter {
    private List<Trip> mTrips = new ArrayList<>();


    @Override
    public int getCount() {
        return mTrips.size();
    }

    @Override
    public Trip getItem(int position) {
        return mTrips.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder h;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.view_grid_trip_item, parent, false);
            h = new ViewHolder(convertView);
            convertView.setTag(h);
        } else {
            h = (ViewHolder) convertView.getTag();
        }
        h.bind(getItem(position));
        return convertView;
    }

    public void setTrips(List<Trip> trips) {
        mTrips = trips;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        @InjectView(R.id.tvNumberTrips)
        TextView tvNumber;

        public ViewHolder(View v) {
            ButterKnife.inject(this, v);
        }

        public void bind(Trip trip) {
            tvNumber.setText(trip.getNumber());
        }
    }

}
