package estafeta.org.testapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import estafeta.org.testapp.R;
import estafeta.org.testapp.pojos.Trip;

public class TripFragment extends BaseFragment {
    @InjectView(R.id.tvTripDetails)
    TextView tvTripDetails;
    private static final String ARG_TRIP = "arg_trip";

    public static TripFragment newInstance(Trip trip) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRIP, trip);
        TripFragment f = new TripFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_trip, container, false);
        ButterKnife.inject(this, v);
        tvTripDetails.setText((getArguments().getSerializable(ARG_TRIP)).toString());
        return v;
    }
}
