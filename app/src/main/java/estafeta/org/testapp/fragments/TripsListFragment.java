package estafeta.org.testapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import estafeta.org.testapp.R;
import estafeta.org.testapp.adapters.TripsListAdapter;
import estafeta.org.testapp.network.NetworkManager;
import estafeta.org.testapp.pojos.Trip;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TripsListFragment extends BaseFragment {
    @InjectView(R.id.gvTrips)
    GridView gvTrips;
    @InjectView(R.id.progressBar)
    View progressBar;
    private NetworkManager mNetworkManager = NetworkManager.get();
    private TripsListAdapter mAdapter = new TripsListAdapter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_trips_list, container, false);
        ButterKnife.inject(this, v);
        gvTrips.setAdapter(mAdapter);
        adjustGridView();
        loadTrips();
        return v;
    }

    private void loadTrips() {
        progressBar.setVisibility(View.VISIBLE);
        mNetworkManager.loadTrips(new Callback<List<Trip>>() {
            @Override
            public void success(List<Trip> trips, Response response) {
                mAdapter.setTrips(trips);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                showToast("ERROR");
            }
        });
    }

    private void adjustGridView() {
        gvTrips.setVerticalSpacing(7);
        gvTrips.setHorizontalSpacing(7);
    }

    @OnItemClick(R.id.gvTrips)
    protected void onTripListItemClick(int position) {

        mActivity.changeFragment(TripFragment.newInstance(mAdapter.getItem(position)), true);
    }
}
