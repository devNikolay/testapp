package estafeta.org.testapp.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import estafeta.org.testapp.activities.BaseActivity;

public abstract class BaseFragment extends Fragment {
    protected BaseActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (BaseActivity) activity;
    }

    protected void showToast(String msg) {
        Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
    }
}
