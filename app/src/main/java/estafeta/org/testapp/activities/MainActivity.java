package estafeta.org.testapp.activities;

import android.os.Bundle;

import estafeta.org.testapp.R;
import estafeta.org.testapp.fragments.BaseFragment;
import estafeta.org.testapp.fragments.TripsListFragment;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    @Override
    protected BaseFragment getInitFragment() {
        return new TripsListFragment();
    }

}
