package estafeta.org.testapp.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(prefix = "m")
public class Trip implements Serializable {
    @SerializedName("Id")
    private int mId;
    @SerializedName("Number")
    private String mNumber;
    @SerializedName("PlannedStartDate")
    private String mPlannedStartDate;
    @SerializedName("PlannedEndDate")
    private String mPlannedEndDate;
    @SerializedName("ActualStartDate")
    private String mActualStartDate;
    @SerializedName("ActualEndDate")
    private String mActualEndDate;
    @SerializedName("CreationDate")
    private String mCreationDate;
    @SerializedName("ExecutorId")
    private String mExecutorId;
    @SerializedName("KindId")
    private int mKindId;
    @SerializedName("IntRowVer")
    private int mIntRowVer;
    @SerializedName("StateId")
    private int mStateId;
    @SerializedName("Vin")
    private String mVin;
    @SerializedName("BrandId")
    private String mBrandId;
    @SerializedName("ExecutorUnitId")
    private String mExecutorUnitId;
    @SerializedName("AssigneeId")
    private String mAssigneeId;
    @SerializedName("GroupTaskId")
    private String mGroupTaskId;
    @SerializedName("TypeId")
    private int mTypeId;
    @SerializedName("IsGroup")
    private boolean mIsGroup;
    @SerializedName("ImplementationTypeId")
    private int mImplementationTypeId;
    @SerializedName("ModelCodeId")
    private String mModelCodeId;
    @SerializedName("AssigneeUnitId")
    private String mAssigneeUnitId;
    @SerializedName("SurveyPointId")
    private String mSurveyPointId;
    @SerializedName("VehiclePosition")
    private int mVehiclePosition;
    @SerializedName("GasAmount")
    private int mGasAmount;
    @SerializedName("IsInTraffic")
    private String mIsInTraffic;
    @SerializedName("Speedometer")
    private int mSpeedometer;
    @SerializedName("VehiclePositionDescription")
    private String mVehiclePositionDescription;
    @SerializedName("SignUserId")
    private String mSignUserId;
    @SerializedName("StateNumber")
    private String mStateNumber;
    @SerializedName("CarrierId")
    private String mCarrierId;
    @SerializedName("ShippingAgentId")
    private String mShippingAgentId;
    @SerializedName("FirstDriverId")
    private String mFirstDriverId;
    @SerializedName("SecondDriverId")
    private String mSecondDriverId;
    @SerializedName("TransportationTypeId")
    private int mTransportationTypeId;
    @SerializedName("TransportId")
    private int mTransportId;
    @SerializedName("FromUnitId")
    private String mFromUnitId;
    @SerializedName("ToUnitId")
    private String mToUnitId;
    @SerializedName("FromUserId")
    private String mFromUserId;
    @SerializedName("ToUserId")
    private String mToUserId;
    @SerializedName("SignUnitId")
    private String mSignUnitId;
}
